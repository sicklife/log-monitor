var WebSocket = require('ws')
var WebSocketServer = require('ws').Server
var wss = new WebSocketServer({port: 8081});
var http = require("http");

var server = http.createServer(function(request,response){
    response.writeHeader(200, {"Content-Type": "text/plain"});
    response.write("Hello\n");
    if (request.url === "/" && request.method === "POST"){
        let body = [];
        request.on('data', (chunk) => {
            body.push(chunk);
        }).on('end', () => {
            body = Buffer.concat(body).toString();
            // at this point, `body` has the entire request body stored in it as a string
            console.log("send mess to web server")
            console.log(body)
            wss.clients.forEach(function each(client) {
                if (client.readyState === WebSocket.OPEN){
                    client.send(body)
                }
        });
        })
    }else{
        response.write("Get request");
    }
    response.end();
}).listen(8001);